#! /usr/bin/env python3

import bpy
import json
import os
import sys
import socket
import json

socket_path = os.path.join(os.environ['BLEND_LOC'], "uds_socket.sock")


def exists(lib):
	if lib.packed_file is None: # it's an external file
		path = lib.filepath
		if path.startswith('//'):
			path = path[2:]
		if path != '' and not os.path.exists(os.path.join(os.path.dirname(os.environ['BLEND_FILE']).encode('utf-8'),  bpy.path.abspath(path).encode('utf-8'))):
			return False
	return True

def list_missing_files():
	ret = []
	for lib in bpy.data.libraries:
		try:
			if exists(lib) == False:
				ret.append(lib.filepath)
		except:
			pass
	for lib in bpy.data.images:
		try:
			if exists(lib) == False:
				ret.append(lib.filepath)
		except:
			pass
	return ret

def has_active_file_output_node():
	try:
		for node in bpy.context.scene.node_tree.nodes:
			if not node.mute and type(node) == bpy.types.CompositorNodeOutputFile or node.type == 'OUTPUT_FILE':
				return True
	except:
		pass
	return False

def can_use_tile():
	use_denoising = False
	for layer in bpy.context.scene.view_layers:
		if layer.cycles.use_denoising == True:
			use_denoising = True
			break
	
	if use_denoising == True:
		return False
	
	if not bpy.context.scene.render.use_compositing:
		return True
	
	# no nodes
	if bpy.context.scene.node_tree is None or bpy.context.scene.node_tree.nodes is None or len(bpy.context.scene.node_tree.nodes.items()) == 0:
		return True

	# todo check if there is active compositing node
	
	return False

def get_samples():
	if bpy.context.scene.cycles.progressive == 'PATH':
		return bpy.context.scene.cycles.samples
	elif bpy.context.scene.cycles.progressive == 'BRANCHED_PATH':
		return bpy.context.scene.cycles.aa_samples
	else:
		# other integrator ??
		return bpy.context.scene.cycles.samples

def total_samples():
	samples = bpy.context.scene.render.resolution_x * bpy.context.scene.render.resolution_y
	samples *= float(bpy.context.scene.render.resolution_percentage) / 100.0
	samples *= float(bpy.context.scene.render.resolution_percentage) / 100.0
	samples *= get_samples()
	if bpy.context.scene.cycles.use_square_samples:
		samples *= get_samples()
	return samples

def list_scripted_driver():
	for o in bpy.data.objects:	
		if o.animation_data is not None:
			for driver in o.animation_data.drivers:
				if driver.driver is not None and driver.driver.type == 'SCRIPTED':
					return True
	return False

def get_info(filename: str):

	filepath = os.path.join(os.environ['BLEND_LOC'], filename)
	bpy.ops.wm.open_mainfile(filepath=filepath)

	info = dict()

	info['scene'] = str(bpy.context.scene.name)
	info['start_frame'] = str(bpy.context.scene.frame_start)
	info['end_frame'] = str(bpy.context.scene.frame_end)
	info['output_file_extension'] = str(bpy.context.scene.render.file_extension)
	info['engine'] = str(bpy.context.scene.render.engine)
	info['resolution_percentage'] = str(bpy.context.scene.render.resolution_percentage)
	info['resolution_x'] = str(bpy.context.scene.render.resolution_x)
	info['resolution_y'] = str(bpy.context.scene.render.resolution_y)
	info['framerate'] = str(bpy.context.scene.render.fps / bpy.context.scene.render.fps_base)
	info['can_use_tile'] = str(can_use_tile())
	info['missing_files'] = list_missing_files()
	info['have_camera'] = bpy.context.scene.camera != None
	info['scripted_driver'] = str(list_scripted_driver())
	info['has_active_file_output_node'] = has_active_file_output_node()

	if info['engine'] == 'CYCLES':
		info['cycles_samples'] = str(total_samples())
		info['cycles_pixel_samples'] = str(get_samples())
	
	return json.dumps(info)


if __name__ == "__main__":
	
	print("installed bpy version:", bpy.app.version)

	print("connecting to UNIX socket...")
	try:
		os.unlink(socket_path)
	except OSError:
		print("creating new socket...")
	
	with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as s:
		s.bind(socket_path)
		s.listen()

		print("socket ready...")
		while True:
			print("listening...")
			conn, _ = s.accept()

			data = conn.recv(1000)
			print(f"got {len(data)}bytes of data")
	
			if data:
				print("analysing data")
				# incoming format: { "filename": "something.blend" }
				fname = json.loads(data.decode()).get("filename")
				print("analysing", fname)
				try:
					info = get_info(filename=fname)
					print("sending result back")
					conn.send(info.encode())
				except RuntimeError as e:
					info = '{ "error": "%s" }' % str(e)
					conn.send(info.encode())
			else:
				print("no data recieved... :(")
			
			conn.close()


print("done")
