#! /usr/bin/env python3 

import os
import sys
import socket
import json

server_soc = 'uds_socket.sock'

# { "filename": "something.blend" }

mess = { "filename": "tests/290-eevee.blend"}

data = json.dumps(mess)

print(data)

with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as s:
    s.connect(server_soc)
    s.send(data.encode())

    answer = s.recv(1000)

    print(answer)
    #print(len(answer))

#print("done")
