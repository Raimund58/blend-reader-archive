# blend-reader-bpy

Blend file reader using Blender as Python module and a UNIX socket for communication.

This repo includes a Python and a PHP implementation for a client.

## How to use

```bash
# create server
git clone https://gitlab.com/zocker-160/blend-reader.git
cd blender-reader
git checkout origin/bpy

docker build src/bpy-server/ -t blendreader
docker run --rm -it -v "$(pwd)":/tmp/blenderfiles blendreader

# run client
php src/BlenderReaderBpy.php tests/280-eevee.blend

# or run one of the testclients
php phpclient.php
python3 pyclient.py
```

## Why

Using this method analysing blend files is faster and less ressource intensive than loading the blend file into a running Blender instance.

Also any software talking to the UNIX socket can be used regardless of programming language.

some performance numbers:

```bash
$ time php phpclient.php 
tests/290-eevee.blend
{"scene": "Scene", "start_frame": "1", "end_frame": "250", "output_file_extension": ".png", "engine": "BLENDER_EEVEE", "resolution_percentage": "100", "resolution_x": "1920", "resolution_y": "1080", "framerate": "24.0", "can_use_tile": "False", "missing_files": [], "have_camera": true, "scripted_driver": "False", "has_active_file_output_node": false}

real    0m0,026s
user    0m0,011s
sys     0m0,000s

$ time python3 pyclient.py 
{"filename": "tests/290-eevee.blend"}
b'{"scene": "Scene", "start_frame": "1", "end_frame": "250", "output_file_extension": ".png", "engine": "BLENDER_EEVEE", "resolution_percentage": "100", "resolution_x": "1920", "resolution_y": "1080", "framerate": "24.0", "can_use_tile": "False", "missing_files": [], "have_camera": true, "scripted_driver": "False", "has_active_file_output_node": false}'

real    0m0,034s
user    0m0,017s
sys     0m0,004s
```

## Protocol

The server expects a JSON with a key called `filename`, which contains the filename of the blend file to analyse. (e.g. `{ "filename": "something.blend" }`)

As answer you either get the information back or an error, which looks like this: `{ "error" : "some error message"}`