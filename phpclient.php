<?php

$server_soc = 'uds_socket.sock';
$data_arr = [ "filename" => "tests/290-eevee.blend" ];

echo $data_arr["filename"].PHP_EOL;

$json_data = json_encode($data_arr);


$socket = socket_create(AF_UNIX, SOCK_STREAM, SOL_SOCKET) or die("Could not create socket");

socket_connect($socket, $server_soc) or die("Could not connect to socket");
socket_send($socket, $json_data, strlen($json_data), 0);

socket_recv($socket, $answer_buff, 1000, 0);
echo $answer_buff.PHP_EOL;

socket_close($socket);
